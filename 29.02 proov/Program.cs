﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace _29._02_proov
{
    class Program
    {
        static void Main(string[] args)
        {

            string filename = "..\\..\\protokoll.txt";

            string[] read = File.ReadAllLines(filename);

            //foreach (var i in read)
            //{
            //    Console.WriteLine(i);
            //}


            string[] nimed = new string[read.Length - 1];
            double[] kiirused = new double[read.Length - 1];

            int kiireim = 0;

            for (int i = 0; i < nimed.Length; i++)
            {
                string[] tykid = read[i + 1].Replace(", ", ",").Split(','); // kaval - ma polegi nii varem teinud
                nimed[i] = tykid[0];
                kiirused[i] = double.Parse(tykid[1]) / double.Parse(tykid[2]);
                Console.WriteLine($"{nimed[i]} kiirus {kiirused[i]:F2}"); // hää oli vahepeal vaadata, kas sain hakkama
                if (kiirused[i] > kiirused[kiireim]) kiireim = i;
            }

            Console.WriteLine($"\nkõikse kiirem on {nimed[kiireim]} oma kiirusega {kiirused[kiireim]:F2}");



            double[] distants = new double[read.Length - 1];

            int kiireim100 = 0;
            int kiireim200 = 0;
            int kiireim400 = 0;


            for (int i = 0; i < nimed.Length; i++)
            {
                string[] tykid2 = read[i + 1].Replace(", ", ",").Split(',');

                distants[i] = double.Parse(tykid2[1]);


                //Console.WriteLine($"nimi:{nimed[i]} distants:{distants[i]} kiirus:{kiirused[i]:F2} (m/s)");


                //PROOV IF FOR tsükli sees:

                if (distants[i] == 100 && kiirused[i] > kiirused[kiireim100]) kiireim100 = i;
                if (distants[i] == 200 && kiirused[i] > kiirused[kiireim200]) kiireim200 = i;
                if (distants[i] == 400 && kiirused[i] > kiirused[kiireim400]) kiireim400 = i;

                // PROOV SWITCH'ga
                //switch (distants[i])
                //{
                //    case 100:
                //        if (kiirused[i] > kiirused[kiireim100]) kiireim100 = i;

                //        break;
                //    case 200:
                //        if (kiirused[i] > kiirused[kiireim200]) kiireim200 = i;

                //        break;

                //    default:
                //        if (kiirused[i] > kiirused[kiireim400]) kiireim400 = i;

                //        break;
                //}


                //PROOV IF'ga

                //if (distants[i] == 100)
                //{
                //    if (kiirused[i] > kiirused[kiireim100]) kiireim100 = i;

                //}
                //else if (distants[i] == 200)
                //{
                //    if (kiirused[i] > kiirused[kiireim200]) kiireim200 = i;

                //}
                //else if (distants[i] == 400)
                //{
                //    if (kiirused[i] > kiirused[kiireim400]) kiireim400 = i;

            }

            Console.WriteLine($"\nkõikse kiirem on {nimed[kiireim200]} oma kiirusega {kiirused[kiireim200]:F2}");
            Console.WriteLine($"\nkõikse kiirem on {nimed[kiireim400]} oma kiirusega {kiirused[kiireim400]:F2}");




        }

    }
}
