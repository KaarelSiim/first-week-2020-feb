﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace _02._03_Ylesanne_1
{
    class Program
    {
        static void Main(string[] args)
        {

            string filename = "..\\..\\protokoll.txt";

            string[] read = File.ReadAllLines(filename);


            // Kontroll, kas tabel töötab normaalselt
            //foreach (var i in read)
            //{
            //    Console.WriteLine(i);
            //}


            string[] nimed = new string[read.Length - 1];
            double[] kiirused = new double[read.Length - 1];
            double[] distantsid = new double[read.Length - 1];

            int kiireim = 0;
            int kiireim100 = -1;
            int kiireim200 = -1;
            int kiireim400 = -1;

            for (int i = 0; i < nimed.Length; i++)
            {
                string[] tykid = read[i + 1].Split(',');
                nimed[i] = tykid[0];
                kiirused[i] = double.Parse(tykid[1]) / double.Parse(tykid[2]);
                distantsid[i] = double.Parse(tykid[1]);

                if (kiirused[i] > kiirused[kiireim]) kiireim = i;

                if (distantsid[i] == 100)
                {
                    if (kiireim100 == -1 || kiirused[i] > kiirused[kiireim100])
                    {
                        kiireim100 = i;

                    }
                }
                else if (distantsid[i] == 200)
                {
                    if (kiireim200 == -1 || kiirused[i] > kiirused[kiireim200])
                    {
                        kiireim200 = i;

                    }

                }
                else if (distantsid[i] == 400)
                {
                    if (kiireim400 == -1 || kiirused[i] > kiirused[kiireim400])
                    {
                        kiireim400 = i;

                    }
                }
                //else Console.WriteLine($"meil on tundmatu distants {distantsid[i]}");


            }

            Console.WriteLine($"Kokkuvõttes on kõige kiirem jooksja {nimed[kiireim]}, tema kiirus on {kiirused[kiireim]}");

            Console.WriteLine();

            /*if (kiireim100 >= 0)*/
            Console.WriteLine($"kiireim 100 m jooksja on {nimed[kiireim100]}, tema kiirus on {kiirused[kiireim100]}");
            /*if (kiireim200 >= 0)*/
            Console.WriteLine($"kiireim 200 m jooksja on {nimed[kiireim200]}, tema kiirus on {kiirused[kiireim200]}");
            /*if (kiireim400 >= 0)*/
            Console.WriteLine($"kiireim 400 m jooksja on {nimed[kiireim400]}, tema kiirus on {kiirused[kiireim400]}");


            //for (int i = 0; i < nimed.Length; i++)
            //{
            //    string[] tykid = read[i + 1].Split(',');
            //    nimed[i] = tykid[0];
            //    kiirused[i] = double.Parse(tykid[1]) / double.Parse(tykid[2]);
            //    distantsid[i] = double.Parse(tykid[1]);
            //    if (distantsid[i] == 200)
            //    {
            //        if (kiirused[i] > kiirused[kiireim200])
            //        {
            //            kiirused[i] = kiireim200;
            //            Console.WriteLine($"kiireim 200 m jooksja on {nimed[kiireim200]}, tema kiirus on {kiirused[kiireim200]}");
            //        }
            //    }
            //    Console.WriteLine($"kiireim 200 m jooksja on {nimed[kiireim200]}, tema kiirus on {kiirused[kiireim200]}");
            //}



        }
    }
}
