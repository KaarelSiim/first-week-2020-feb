﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _27._02_ver3
{
    class Program
    {
        static void Main(string[] args)
        {

            bool muutuja = true;

            do
            {
                Console.Write("Mis nädalapäev on?");
                String paev = Console.ReadLine().ToLower();

                switch (paev)
                {
                    case "esmaspäev":
                        Console.WriteLine("täna alkoholi ei joo");
                        break;

                    case "teisipäev":
                        goto case "esmaspäev";

                    case "kolmapäev":
                        Console.WriteLine("täna joome ühe õlle");
                        break;

                    case "neljapäev":
                        goto case "reede";

                    case "reede":
                        Console.WriteLine("täna joome veini");
                        muutuja = false;
                        break;

                    default:
                        Console.WriteLine("nädalavahetus");
                        break;

                }
            } while (muutuja);
        }
    }
}
