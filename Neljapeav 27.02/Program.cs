﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Neljapeav_27._02
{
    class Program
    {
        static void Main(string[] args)
        {
            int age = 90;
            string name = "Kustav";
            Console.WriteLine($" {name} vanus on {age} aastat");






            if (DateTime.Now.DayOfWeek == DayOfWeek.Thursday) // tingimuslik lause
            {
                Console.WriteLine("täna ei ole saunapäev");


                // IF lause puhul rakendatakse looksulgude vahel olevad tegevused
            }
            else
            {
                int vihtadeArv = 2;
                Console.WriteLine($"võtame kaasa {vihtadeArv} vihtasid");

                if (vihtadeArv == 0) Console.WriteLine("täna ei vihtle");

                // need laused, siin plokis täidetakse siis, kui IF tagune tingimus on FALSE
            }



            if (DateTime.Now.DayOfWeek == DayOfWeek.Saturday)
            {
                // siin võib olla täiendav IF ja else

                // laupäevased laused
            }
            else if (DateTime.Now.DayOfWeek == DayOfWeek.Sunday)
            {
                // pühapäevased laused

            }
            else
            {

                // muud päevad

            }


            // muutuja skoop
            string see = "see on protseduuri tasemel muutuja";
            {
                int kohalik = 5;
            }

            {
                string kohalik = "kaheksa";
            }

            // if lõpeb siin



            // siit peaks olema näidisülesanne, vaata araldi Solutionit 27. 02 testylesanne 1

            
                   



            
        }


    }
}


