﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02._03_ylesanne_3
{
    class Program
    {
        static void Main(string[] args)
        {
            Random r = new Random();
            List<int> randomNumbers = new List<int>();

            for (int i = 0; i < 52; i++)
            {
                int number;

                do number = r.Next(1,52);
                while (randomNumbers.Contains(number));

                randomNumbers.Add(number);
            }


            for (int i = 0; i < 52; i++)
            {
                Console.Write($"{randomNumbers[i]:00}{(i % 13 == 12 ? "\n" : "\t")}"); 
            }

        }
    }
}
