﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace _28._02_ylesanne_4
{
    class Program
    {
        static void Main(string[] args)
        {
            string filename = @"..\..\TextFile1.txt";
            string sisu = File.ReadAllText(filename);
            Console.WriteLine(sisu);

            string[] loetudRead = File.ReadAllLines(filename);
            for (int i = 0; i < loetudRead.Length; i++)
            {
                Console.WriteLine($"rida {i} - {loetudRead [i]}");
            }



        }
    }
}
