﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02._03_ylesanne_2
{
    class Program
    {
        static void Main(string[] args)
        {

            List<int> kaardiPakk = Enumerable.Range(1, 52).ToList();


            Console.WriteLine("\nSegamata Pakk\n");


            Random r = new Random();

            // esimene variant

            for (int i = 0; i < 52; i++)
            {
                Console.Write($"{kaardiPakk[i]:00}{(i%13 == 12 ? "\n" : "\t")}");
            }

            List<int> segatudPakk = new List<int>();
            while (kaardiPakk.Count > 0)
            {
                int mitmes = r.Next(kaardiPakk.Count);      // leiame juhusliku kaardi (number 0 ... kaartide arv)
                segatudPakk.Add(kaardiPakk[mitmes]);                    // paneme ta teise pakki
                kaardiPakk.RemoveAt(mitmes);                // kustutame ta esimesest paksit ära
            }
            Console.WriteLine("\nSegatud Pakk\n" );
            for (int i = 0; i < 52; i++)
            {
                Console.Write($"{segatudPakk[i]:00}{(i%13 == 12 ? "\n" : "\t")}");
            }

            // teine variant


            segatudPakk = new List<int>();

            Random uusR = new Random();

            for (int i = 0; i < 52; i++)
                {
                int mitmes = r.Next(1, 52);
                int järgmine = uusR.Next(1, 52);

                segatudPakk.Add(mitmes);
                    if (segatudPakk.Contains(mitmes))
                    {
                        segatudPakk.Add(järgmine);

                    }
                } 
          
            Console.WriteLine("\nSegatud Pakk 2\n");
            for (int i = 0; i < 52; i++)
            {
                Console.Write($"{segatudPakk[i]:00}{(i % 13 == 12 ? "\n" : "\t")}");
            }



        }
    }
}
