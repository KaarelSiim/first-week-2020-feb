﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt_1
{
    class Program
    {
        static void Main(string[] args)
        {
            // see on 1. muudatus
            // see on 2. muudatus
            Console.WriteLine("Kas töötab?");

            int arv1 = 7;
            decimal arv2 = 10.777M;
            Console.WriteLine("arvud on sellised {0} ja {1:F2}", arv1, arv2);



            #region Peidetav lõik
            String tulemus = string.Format("arvud on sellised {0} ja {1:F2}", arv1, arv2);
            Console.WriteLine(tulemus);

            Console.WriteLine($"arvud on sellised {arv1} ja {arv2:F4}");

            tulemus = $"arvud on sellised {arv1} ja {arv2:F4}";
            Console.WriteLine(tulemus);

            var v = 7.0021;
            Console.WriteLine(v.GetType().Name);

            var suva = new { }

            #endregion




        }
    }
}
