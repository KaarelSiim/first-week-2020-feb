﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _27._02_testylesanne_IF
{
    class Program
    {
        static void Main(string[] args)
        {



            string paevaOsa;
            do
            {
                Console.Write("Milline päevaosa hetkel on?");
                paevaOsa = Console.ReadLine().ToLower();
                

                if (paevaOsa == "öö")
                {
                    Console.WriteLine("maga edasi");

                }

                else if (paevaOsa == "hommik")
                {
                    Console.WriteLine("ärka üles");
                }
                else
                {
                    Console.WriteLine("mina selliseid päevaosasid ei teagi");
                }
            } while (paevaOsa != "hommik");
        }
    }
}
