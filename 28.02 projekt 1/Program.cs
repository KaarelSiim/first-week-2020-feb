﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _28._02_projekt_1
{
    class Program
    {
        static void Main(string[] args)
        {

            Random r = new Random(); // see on juhusliku arvu genereerija andmetüüp
            




            int i = r.Next(); // randomi käest saab küsida juhuslikke täisarve

            Console.WriteLine(i);

            i = r.Next(100);
            Console.WriteLine(i);       // juhuslikke arve kuni sajani

            i = r.Next(10, 20);         // juhuslikke arve 10 kuni 20 sajani
            Console.WriteLine(i);

            double d = r.NextDouble();      // juhuslikke double'd vahemikus 0 kuni 1
            Console.WriteLine(d);


        }
    }
}
