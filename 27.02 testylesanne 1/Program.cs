﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _27._02_testylesanne_1
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.Write("Kas soovite IF (i) või Switch (s) või elvis (e)?");
            string foorivarvus = Console.ReadLine();


            switch (foorivarvus)
            {
                case "i":
                case "I":
                    
                    
                    String fooriTuli;
                    do
                    {

                        Console.Write("Mis värvi sa näed?");

                        fooriTuli = Console.ReadLine();



                        if (fooriTuli == "roheline" || fooriTuli == "green")
                        {
                            Console.WriteLine("Sõida edasi");
                        }
                        else if (fooriTuli == "kollane")
                        {
                            Console.WriteLine("pidurda");
                        }
                        else if (fooriTuli == "punane")
                        {
                            Console.WriteLine("stopp");
                        }
                        else
                        {
                            Console.WriteLine("pole aimugi");
                        }
                    } while (fooriTuli != "roheline");



                    break;



                case "s":
                case "S":

                    Console.Write("Mis värvi tuli on valgusfooris?");

                    string foorivarv = Console.ReadLine();

                    switch (foorivarv)
                    {
                        case "roheline":
                        case "green":
                            Console.WriteLine("Sõida edasi");
                            break;

                        case "kollane":
                        case "yellow":
                            Console.WriteLine("Pidurda");
                            break;

                        case "punane":
                        case "red":
                            Console.WriteLine("Stopp");
                            break;
                        default:
                            Console.WriteLine("siis ma ei tea");
                            break;

                    }
                    break;

                case "e":
                case "E":

                    Console.Write("Mis värvi tuli on valgusfooris näed?");
                    string fooriVarvused = Console.ReadLine();

                    Console.WriteLine(
                        fooriVarvused == "roheline" || fooriVarvused == "green" ? "sõida" :
                         fooriVarvused == "kollane" || fooriVarvused == "yellow" ? "pidurda" :
                          fooriVarvused == "punane" || fooriVarvused == "red" ? "stopp" :
                          "ei tea midagi"
                        );
                    break;


            }
        }
    }
}
