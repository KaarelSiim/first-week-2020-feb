﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _27._02_proov
{
    class Program
    {
        static void Main(string[] args)
        {
            int sum = 0;

            for (int i = 1; i <= 100 ; i++)
            {
                sum = sum + i;

            }

            Console.WriteLine($"arvude 1 kuni 100 summa on {sum}");
        }
    }
}
