﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _27._02_Elvis
{
    class Program
    {
        static void Main(string[] args)
        {
            bool jätka = false;
            do
            {

                Console.Write("Milline päevaosa hetkel on?");
                string paevaOsa = Console.ReadLine().ToLower();

                Console.WriteLine((jätka = (paevaOsa == "öö")) ? "maga edasi" : paevaOsa == "hommik" ? "ärka üles" : paevaOsa == "päev" ? "tee tööd" : "nüüd ei saa midagi aru");

            } while (!jätka);



        }
    }
}
